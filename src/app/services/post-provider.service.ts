import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PostProviderService {
  PHP_API_LYDIA_SERVER = "https://homologation.lydia-app.com/";

  constructor(private httpClient:HttpClient) { }

 
   postData(data:any, typePost:string):Observable<any> {

     const httpOptions = {
       headers: new HttpHeaders()}
 
       httpOptions.headers.append('Content-Type', 'application/json');
     return this.httpClient.post(this.PHP_API_LYDIA_SERVER+typePost, JSON.stringify(data), httpOptions)
     .pipe(map((response: any) => response));
 
   }
}
