import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostProviderService } from 'src/app/services/post-provider.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  fakeUserLogin : any = {
    vendor_token : "556728ae65bd6264713182",
    phone : "0650939350",
    password : "hello_world",
    notify : "no",
  };

  fakeUserRegister: any = {
      vendor_token : "556728ae65bd6264713182",
      phone : "0650939350",
      password : "hello_world",
      firstname : "Simon",
      lastname : "Watiau",
      email : "test@email.com",
      merchant_ident : "abc-123",
      merchant_ident_second : "5555903"
 };

  constructor(private postProviderService : PostProviderService, private snackBar: MatSnackBar) {}

  ngOnInit() {
  }
  login(){
    this.postProviderService.postData(this.fakeUserLogin,'api/auth/login').subscribe(data=>{
      console.log(data);
      this.openSnackBar(data.message);
    });
  }
  register(){
    this.postProviderService.postData(this.fakeUserRegister,'api/auth/register').subscribe(data=>{
      console.log(data);
      this.openSnackBar(data.message);
    });
  }






  openSnackBar(message: string) {
    this.snackBar.open(message, "OK", {duration: 2000});
  }

  
}
